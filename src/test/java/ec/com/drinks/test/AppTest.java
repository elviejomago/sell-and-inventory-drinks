package ec.com.drinks.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import ec.com.drinks.enumeration.IngredientEnum;
import ec.com.drinks.exception.NoEnoughIngredientsException;
import ec.com.drinks.services.InventoryService;
import ec.com.drinks.services.SellService;
import ec.com.drinks.util.ConstantUtil;
import ec.com.drinks.util.MenuUtil;

/**
 * @author jpatino
 * @version 1.0
 * 
 * Class of test that contains methods to test different functionalities of the application.
 */
public class AppTest {

	@Test
	public void testPrintInventory() {
		InventoryService inventoryService = new InventoryService();

		inventoryService.printInventory();
		assertNotNull(inventoryService.getIngredientsInventory());
	}

	@Test
	public void testSellBananaDrinkMedium() {
		InventoryService inventoryService = new InventoryService();
		SellService sellService = new SellService();

		String message = null;
		try {
			message = sellService.sellDrink(MenuUtil.getSizeFromOption("2"), IngredientEnum.FRUIT_BANANA, inventoryService);
		} catch (NoEnoughIngredientsException e) {}
		assertEquals(ConstantUtil.SUCCESS_SALE, message);
	}

	@Test
	public void testSellDrinkMixedDrinkMedium() {
		InventoryService inventoryService = new InventoryService();
		SellService sellService = new SellService();

		String message = null;
		try {
			message = sellService.sellDrink(MenuUtil.getSizeFromOption("2"), IngredientEnum.FRUIT_BANANA_MANGO, inventoryService);
		} catch (NoEnoughIngredientsException e) {}
		assertEquals(ConstantUtil.SUCCESS_SALE, message);
	}

	@Test
	public void testNoEnoughIngredients() {
		InventoryService inventoryService = new InventoryService();
		SellService sellService = new SellService();

		String message = null;
		try {
			message = sellService.sellDrink(MenuUtil.getSizeFromOption("3"), IngredientEnum.FRUIT_STRAWBERRY, inventoryService);
			message = sellService.sellDrink(MenuUtil.getSizeFromOption("3"), IngredientEnum.FRUIT_BANANA, inventoryService);
			message = sellService.sellDrink(MenuUtil.getSizeFromOption("3"), IngredientEnum.FRUIT_MANGO, inventoryService);
			message = sellService.sellDrink(MenuUtil.getSizeFromOption("3"), IngredientEnum.FRUIT_STRAWBERRY, inventoryService);
			message = sellService.sellDrink(MenuUtil.getSizeFromOption("3"), IngredientEnum.FRUIT_BANANA, inventoryService);
			message = sellService.sellDrink(MenuUtil.getSizeFromOption("3"), IngredientEnum.FRUIT_MANGO, inventoryService);
			message = sellService.sellDrink(MenuUtil.getSizeFromOption("3"), IngredientEnum.FRUIT_STRAWBERRY, inventoryService);
			message = sellService.sellDrink(MenuUtil.getSizeFromOption("3"), IngredientEnum.FRUIT_BANANA, inventoryService);
			message = sellService.sellDrink(MenuUtil.getSizeFromOption("3"), IngredientEnum.FRUIT_MANGO, inventoryService);
			message = sellService.sellDrink(MenuUtil.getSizeFromOption("3"), IngredientEnum.FRUIT_STRAWBERRY, inventoryService);
			message = sellService.sellDrink(MenuUtil.getSizeFromOption("3"), IngredientEnum.FRUIT_BANANA, inventoryService);
			message = sellService.sellDrink(MenuUtil.getSizeFromOption("3"), IngredientEnum.FRUIT_MANGO, inventoryService);
			message = sellService.sellDrink(MenuUtil.getSizeFromOption("3"), IngredientEnum.FRUIT_STRAWBERRY, inventoryService);
			message = sellService.sellDrink(MenuUtil.getSizeFromOption("3"), IngredientEnum.FRUIT_BANANA, inventoryService);
			message = sellService.sellDrink(MenuUtil.getSizeFromOption("3"), IngredientEnum.FRUIT_MANGO, inventoryService);
		} catch (NoEnoughIngredientsException e) {}
		assertEquals(ConstantUtil.NO_ENOUGH_INGREDIENTS_MESSAGE+" - "+IngredientEnum.ICE, message);
	}

	@Test
	public void testReportDailySales() {
		InventoryService inventoryService = new InventoryService();
		SellService sellService = new SellService();

		try {
			sellService.sellDrink(MenuUtil.getSizeFromOption("2"), IngredientEnum.FRUIT_BANANA, inventoryService);
			sellService.printReportDailySales();
		} catch (NoEnoughIngredientsException e) {}
		assertNotNull(sellService.getReportDailySales());
	}

}
