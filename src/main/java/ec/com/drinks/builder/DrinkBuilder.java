package ec.com.drinks.builder;

import ec.com.drinks.enumeration.SizeEnum;
import ec.com.drinks.model.Drink;

/**
 * @author jpatino
 * @version 1.0
 * 
 * Abstract Class that allows you to build a diferents drinks.
 */
public abstract class DrinkBuilder {

	private final int QUANTITY_ML_ICE_SMALL = 30;
	private final int QUANTITY_ML_ICE_MEDIUM = 60;
	private final int QUANTITY_ML_ICE_LARGE = 90;

	private final int QUANTITY_ML_CONDENCED_MILK_SMALL = 20;
	private final int QUANTITY_ML_CONDENCED_MILK_MEDIUM = 40;
	private final int QUANTITY_ML_CONDENCED_MILK_LARGE = 60;

	private final int QUANTITY_GRAME_SUGAR_SMALL = 8;
	private final int QUANTITY_GRAME_SUGAR_MEDIUM = 16;
	private final int QUANTITY_GRAME_SUGAR_LARGE = 24;
	
	protected Drink drink;

	public abstract void addBlendedFruit();

	public void newDrink(SizeEnum size) {
		drink = new Drink(size);
	}

	public Drink getDrink() {
		return drink;
	}

	public void addIce() {
		switch (drink.getSize()) {
		case SMALL:
			drink.setIce(QUANTITY_ML_ICE_SMALL);
			break;

		case MEDIUM:
			drink.setIce(QUANTITY_ML_ICE_MEDIUM);
			break;

		case LARGE:
			drink.setIce(QUANTITY_ML_ICE_LARGE);
			break;

		default:
			break;
		}
		
	}

	public void addSugar() {
		switch (drink.getSize()) {
		case SMALL:
			drink.setSugar(QUANTITY_GRAME_SUGAR_SMALL);
			break;

		case MEDIUM:
			drink.setSugar(QUANTITY_GRAME_SUGAR_MEDIUM);
			break;

		case LARGE:
			drink.setSugar(QUANTITY_GRAME_SUGAR_LARGE);
			break;

		default:
			break;
		}
	}

	public void addCondencedMilk() {
		switch (drink.getSize()) {
		case SMALL:
			drink.setCondensedMilk(QUANTITY_ML_CONDENCED_MILK_SMALL);
			break;

		case MEDIUM:
			drink.setCondensedMilk(QUANTITY_ML_CONDENCED_MILK_MEDIUM);
			break;

		case LARGE:
			drink.setCondensedMilk(QUANTITY_ML_CONDENCED_MILK_LARGE);
			break;

		default:
			break;
		}
	}

}
