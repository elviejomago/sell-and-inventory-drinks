package ec.com.drinks.builder;

import ec.com.drinks.enumeration.IngredientEnum;

/**
 * @author jpatino
 * @version 1.0
 * 
 * Class that allows you to build a Mango drink.
 */
public class MangoDrinkBuilder extends DrinkBuilder {

	private final int QUANTITY_GRAME_MANGO_DRINK_SMALL = 70;
	private final int QUANTITY_GRAME_MANGO_DRINK_MEDIUM = 140;
	private final int QUANTITY_GRAME_MANGO_DRINK_LARGE = 210;

	@Override
	public void addBlendedFruit() {
		drink.setFruitType(IngredientEnum.FRUIT_MANGO);

		switch (drink.getSize()) {
		case SMALL:
			drink.setFruit(QUANTITY_GRAME_MANGO_DRINK_SMALL);
			break;

		case MEDIUM:
			drink.setFruit(QUANTITY_GRAME_MANGO_DRINK_MEDIUM);
			break;

		case LARGE:
			drink.setFruit(QUANTITY_GRAME_MANGO_DRINK_LARGE);
			break;

		default:
			break;
		}
	}

}
