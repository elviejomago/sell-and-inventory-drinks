package ec.com.drinks.builder;

import ec.com.drinks.enumeration.IngredientEnum;

/**
 * @author jpatino
 * @version 1.0
 * 
 * Class that allows you to build a Strawberry drink.
 */
public class StrawberryDrinkBuilder extends DrinkBuilder {

	private final int QUANTITY_GRAME_STRAWBERRY_DRINK_SMALL = 50;
	private final int QUANTITY_GRAME_STRAWBERRY_DRINK_MEDIUM = 100;
	private final int QUANTITY_GRAME_STRAWBERRY_DRINK_LARGE = 150;

	@Override
	public void addBlendedFruit() {
		drink.setFruitType(IngredientEnum.FRUIT_STRAWBERRY);

		switch (drink.getSize()) {
		case SMALL:
			drink.setFruit(QUANTITY_GRAME_STRAWBERRY_DRINK_SMALL);
			break;

		case MEDIUM:
			drink.setFruit(QUANTITY_GRAME_STRAWBERRY_DRINK_MEDIUM);
			break;

		case LARGE:
			drink.setFruit(QUANTITY_GRAME_STRAWBERRY_DRINK_LARGE);
			break;

		default:
			break;
		}
	}

}
