package ec.com.drinks.builder;

import ec.com.drinks.enumeration.IngredientEnum;

/**
 * @author jpatino
 * @version 1.0
 * 
 * Class that allows you to build a Banana drink.
 */
public class BananaDrinkBuilder extends DrinkBuilder {

	private final int QUANTITY_GRAME_BANANA_DRINK_SMALL = 60;
	private final int QUANTITY_GRAME_BANANA_DRINK_MEDIUM = 120;
	private final int QUANTITY_GRAME_BANANA_DRINK_LARGE = 180;
	
	@Override
	public void addBlendedFruit() {
		drink.setFruitType(IngredientEnum.FRUIT_BANANA);

		switch (drink.getSize()) {
		case SMALL:
			drink.setFruit(QUANTITY_GRAME_BANANA_DRINK_SMALL);
			break;

		case MEDIUM:
			drink.setFruit(QUANTITY_GRAME_BANANA_DRINK_MEDIUM);
			break;

		case LARGE:
			drink.setFruit(QUANTITY_GRAME_BANANA_DRINK_LARGE);
			break;

		default:
			break;
		}
	}

}
