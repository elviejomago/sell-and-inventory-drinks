package ec.com.drinks.builder;

import ec.com.drinks.enumeration.IngredientEnum;

/**
 * @author jpatino
 * @version 1.0
 * 
 * Class that allows you to build a mixed drink Banana - Mango.
 */
public class BananaMangoDrinkBuilder extends DrinkBuilder {

	private final int QUANTITY_GRAME_BANANA_MIXED_DRINK_SMALL = 30;
	private final int QUANTITY_GRAME_BANANA_MIXED_DRINK_MEDIUM = 60;
	private final int QUANTITY_GRAME_BANANA_MIXED_DRINK_LARGE = 90;
	private final int QUANTITY_GRAME_MANGO_MIXED_DRINK_SMALL = 35;
	private final int QUANTITY_GRAME_MANGO_MIXED_DRINK_MEDIUM = 70;
	private final int QUANTITY_GRAME_MANGO_MIXED_DRINK_LARGE = 105;

	@Override
	public void addBlendedFruit() {
		drink.setFruitType(IngredientEnum.FRUIT_BANANA_MANGO);

		switch (drink.getSize()) {
		case SMALL:
			drink.setFruit(QUANTITY_GRAME_BANANA_MIXED_DRINK_SMALL);
			drink.setFruitMixed(QUANTITY_GRAME_MANGO_MIXED_DRINK_SMALL);
			break;

		case MEDIUM:
			drink.setFruit(QUANTITY_GRAME_BANANA_MIXED_DRINK_MEDIUM);
			drink.setFruitMixed(QUANTITY_GRAME_MANGO_MIXED_DRINK_MEDIUM);
			break;

		case LARGE:
			drink.setFruit(QUANTITY_GRAME_BANANA_MIXED_DRINK_LARGE);
			drink.setFruitMixed(QUANTITY_GRAME_MANGO_MIXED_DRINK_LARGE);
			break;

		default:
			break;
		}
	}

}
