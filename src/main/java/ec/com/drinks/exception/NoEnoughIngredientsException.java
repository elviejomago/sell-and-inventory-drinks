package ec.com.drinks.exception;

/**
 * @author jpatino
 * @version 1.0
 * 
 * Exception that is thrown when there are not enough ingredients.
 */
public class NoEnoughIngredientsException extends Exception{

	private static final long serialVersionUID = -5401498301486094378L;

    public NoEnoughIngredientsException() {
    }

    public NoEnoughIngredientsException(String message) {
        super(message);
    }

    public NoEnoughIngredientsException(Throwable cause) {
        super(cause);
    }

    public NoEnoughIngredientsException(String message, Throwable cause) {
        super(message, cause);
    }

}
