package ec.com.drinks.util;

import ec.com.drinks.enumeration.SizeEnum;

/**
 * @author jpatino
 * @version 1.0
 * 
 * Utility that contains methods to generate the menu.
 */
public class MenuUtil {
	
	public static void generateMainMenu() {
		System.out.println("************************************************************************");
		System.out.println("************** SELL AND INVENTORY DRINKS *******************************");
		System.out.println("************************************************************************");
		System.out.println("********************* MAIN MENU *****************************************");
		System.out.println("\n");
		System.out.println("Enter the number of the option you want to run.");
		System.out.println("\n");
		
		System.out.println("1. Current Inventory of Ingredients");
		System.out.println("2. Sell Banana Drink");
		System.out.println("3. Sell Mango Drink");
		System.out.println("4. Sell Strawberry Drink");
		System.out.println("5. Sell Banana - Mango Drink");
		System.out.println("6. Report of daily sales");
		System.out.println("7. Exit");
		System.out.println("");
	}
	
	public static void generateSizeMenu() {
		System.out.println("\n");
		System.out.println("select the size of the drink.");
		System.out.println("\n");
		
		System.out.println("1. SMALL (100 ml)");
		System.out.println("2. MEDIUM (200 ml)");
		System.out.println("3. LARGE (300 ml)");
		System.out.println("0. Return");
		System.out.println("");
	}

	public static SizeEnum getSizeFromOption(String option) {
		SizeEnum size = null;
		switch (option) {
		case "1":
			size = SizeEnum.SMALL;
			break;

		case "2":
			size = SizeEnum.MEDIUM;
			break;
			
		case "3":
			size = SizeEnum.LARGE;
			break;
			
		default:
			break;
		}
		
		return size;
	}	
}
