package ec.com.drinks.util;

/**
 * @author jpatino
 * @version 1.0
 * 
 * Utility that contains constants to use in application.
 */
public class ConstantUtil {

	public static final String FORMAT_SPACE_COLUMNS = "%-20s";
	public static final String NEW_LINE = "\n";
	public static final String RETURN_MESSAGE = "\nEnter 0 to return to the menu ...";
	public static final String OPTION_NO_FOUND_MESSAGE = "\nThe option entered does not exist, try again ...";
	public static final String EXIT_MESSAGE = "\nThank you, good day ...\n";
	public static final String NO_ENOUGH_INGREDIENTS_MESSAGE = "\nNot enough ingredients to make the drink, enter 0 to return to the menu ...\n";
	public static final String NO_ENOUGH_INGREDIENTS_FOUR_DRINKS_MESSAGE = "\nWARNING - Not enough ingredients by four Drinks of this size {size}, enter 0 to return to the menu ...";
	public static final String SUCCESS_SALE = "\nSuccessful Sale, enter 0 to return to the menu ...";

}
