package ec.com.drinks;

import java.util.Scanner;

import ec.com.drinks.enumeration.IngredientEnum;
import ec.com.drinks.services.InventoryService;
import ec.com.drinks.services.SellService;
import ec.com.drinks.util.ConstantUtil;
import ec.com.drinks.util.MenuUtil;

/**
 * @author jpatino
 * @version 1.0
 * 
 *          Initial class that launches the sale and inventory application.
 */
public class App {
	public static void main(String[] args) {

		Scanner keyboard = new Scanner(System.in);

		try {
			InventoryService inventoryService = new InventoryService();
			SellService sellService = new SellService();
			
			MenuUtil.generateMainMenu();
			String option = keyboard.nextLine();
			String message = "";
			while (true) {
				switch (option) {
				case "0":
					MenuUtil.generateMainMenu();
					option = keyboard.nextLine();
					break;
				case "1":
					inventoryService.printInventory();
					option = keyboard.nextLine();
					break;
				case "2":
					MenuUtil.generateSizeMenu();
					option = keyboard.nextLine();
					message = sellService.sellDrink(MenuUtil.getSizeFromOption(option), IngredientEnum.FRUIT_BANANA, inventoryService);
					System.out.println(message);
					option = keyboard.nextLine();
					break;
				case "3":
					MenuUtil.generateSizeMenu();
					option = keyboard.nextLine();
					message = sellService.sellDrink(MenuUtil.getSizeFromOption(option), IngredientEnum.FRUIT_MANGO, inventoryService);
					System.out.println(message);
					option = keyboard.nextLine();
					break;
				case "4":
					MenuUtil.generateSizeMenu();
					option = keyboard.nextLine();
					message = sellService.sellDrink(MenuUtil.getSizeFromOption(option), IngredientEnum.FRUIT_STRAWBERRY, inventoryService);
					System.out.println(message);
					option = keyboard.nextLine();
					break;
				case "5":
					MenuUtil.generateSizeMenu();
					option = keyboard.nextLine();
					message = sellService.sellDrink(MenuUtil.getSizeFromOption(option), IngredientEnum.FRUIT_BANANA_MANGO, inventoryService);
					System.out.println(message);
					option = keyboard.nextLine();
					break;
				case "6":
					sellService.printReportDailySales();
					option = keyboard.nextLine();
					break;
				case "7":
					System.out.println(ConstantUtil.EXIT_MESSAGE);
					System.exit(1);
					break;

				default:
					System.out.println(ConstantUtil.OPTION_NO_FOUND_MESSAGE);
					option = keyboard.nextLine();
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		} finally {
			keyboard.close();
		}
	}

}
