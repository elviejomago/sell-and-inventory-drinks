package ec.com.drinks.model;

import ec.com.drinks.enumeration.IngredientEnum;
import ec.com.drinks.enumeration.SizeEnum;

/**
 * @author jpatino
 * @version 1.0
 * 
 * Class that represents the attributes of a drink.
 */
public class Drink {

	private int ice;
	private int sugar;
	private int condensedMilk;
	private int fruit;
	private int fruitMixed;
	private IngredientEnum fruitType;
	private SizeEnum size;
	private double price;
	
	public Drink(SizeEnum size) {
		this.size = size;
		this.price = 0.0;
	}
	
	public int getIce() {
		return ice;
	}
	public void setIce(int ice) {
		this.ice = ice;
	}
	public int getSugar() {
		return sugar;
	}
	public void setSugar(int sugar) {
		this.sugar = sugar;
	}
	public int getCondensedMilk() {
		return condensedMilk;
	}
	public void setCondensedMilk(int condensedMilk) {
		this.condensedMilk = condensedMilk;
	}
	public int getFruit() {
		return fruit;
	}
	public void setFruit(int fruit) {
		this.fruit = fruit;
	}
	public IngredientEnum getFruitType() {
		return fruitType;
	}
	public void setFruitType(IngredientEnum fruitType) {
		this.fruitType = fruitType;
	}
	public SizeEnum getSize() {
		return size;
	}
	public void setSize(SizeEnum size) {
		this.size = size;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getFruitMixed() {
		return fruitMixed;
	}

	public void setFruitMixed(int fruitMixed) {
		this.fruitMixed = fruitMixed;
	}
	
}
