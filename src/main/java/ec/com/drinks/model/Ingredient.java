package ec.com.drinks.model;

import ec.com.drinks.enumeration.IngredientEnum;
import ec.com.drinks.enumeration.UnitEnum;

/**
 * @author jpatino
 * @version 1.0
 * 
 * Class that represents the attributes of a ingredient.
 */
public class Ingredient {
	private IngredientEnum code;
	private UnitEnum unit;
	private int quantity;
	private double priceByUnit;
	
	public Ingredient(IngredientEnum code, UnitEnum unit, int quantity, double priceByUnit) {
		super();
		this.code = code;
		this.unit = unit;
		this.quantity = quantity;
		this.priceByUnit = priceByUnit;
	}

	public IngredientEnum getCode() {
		return code;
	}

	public UnitEnum getUnit() {
		return unit;
	}

	public int getQuantity() {
		return quantity;
	}

	public double getPriceByUnit() {
		return priceByUnit;
	}
	
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}

