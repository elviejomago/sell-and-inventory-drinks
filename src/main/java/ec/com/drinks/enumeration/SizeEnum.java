package ec.com.drinks.enumeration;

/**
 * @author jpatino
 * @version 1.0
 * 
 * Enumeration that represents the sizes of drinks.
 */
public enum SizeEnum {
	SMALL,
	MEDIUM,
	LARGE
}
