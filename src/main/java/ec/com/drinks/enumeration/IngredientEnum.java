package ec.com.drinks.enumeration;

/**
 * @author jpatino
 * @version 1.0
 * 
 * Enumeration that represents the types of ingredients.
 */
public enum IngredientEnum {
	FRUIT_BANANA,
	FRUIT_MANGO,
	FRUIT_STRAWBERRY,
	FRUIT_BANANA_MANGO,
	ICE,
	SUGAR,
	CONDENSED_MILK
}
