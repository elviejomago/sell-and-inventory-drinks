package ec.com.drinks.enumeration;

/**
 * @author jpatino
 * @version 1.0
 * 
 * Enumeration that represents the units of ingredients.
 */
public enum UnitEnum {
	ML,
	GRAM
}
