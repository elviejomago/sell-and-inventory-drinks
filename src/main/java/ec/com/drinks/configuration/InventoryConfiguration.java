package ec.com.drinks.configuration;

import java.util.HashMap;
import java.util.Map;

import ec.com.drinks.enumeration.IngredientEnum;
import ec.com.drinks.enumeration.UnitEnum;
import ec.com.drinks.model.Ingredient;

/**
 * @author jpatino
 * @version 1.0
 * 
 * Initial configuration of ingredients inventory.
 */
public class InventoryConfiguration {

	private Map<IngredientEnum, Ingredient> ingredientsInventory;
	
	protected InventoryConfiguration() {
		super();
		initialize();
	}
	
	private void initialize(){
		ingredientsInventory = new HashMap<IngredientEnum, Ingredient>();
		ingredientsInventory.put(IngredientEnum.ICE, generateIce());
		ingredientsInventory.put(IngredientEnum.SUGAR, generateSugar());
		ingredientsInventory.put(IngredientEnum.CONDENSED_MILK, generateCondensedMilk());
		ingredientsInventory.put(IngredientEnum.FRUIT_BANANA, generateFruitBanana());
		ingredientsInventory.put(IngredientEnum.FRUIT_MANGO, generateFruitMango());
		ingredientsInventory.put(IngredientEnum.FRUIT_STRAWBERRY, generateFruitStrawberry());
	}

	private Ingredient generateIce() {
		return new Ingredient(IngredientEnum.ICE, UnitEnum.ML, 500, 0.01);
	}

	private Ingredient generateSugar() {
		return new Ingredient(IngredientEnum.SUGAR, UnitEnum.GRAM, 500, 0.02);
	}

	private Ingredient generateCondensedMilk() {
		return new Ingredient(IngredientEnum.CONDENSED_MILK, UnitEnum.ML, 500, 0.05);
	}

	private Ingredient generateFruitBanana() {
		return new Ingredient(IngredientEnum.FRUIT_BANANA, UnitEnum.GRAM, 500, 0.10);
	}

	private Ingredient generateFruitMango() {
		return new Ingredient(IngredientEnum.FRUIT_MANGO, UnitEnum.GRAM, 700, 0.15);
	}

	private Ingredient generateFruitStrawberry() {
		return new Ingredient(IngredientEnum.FRUIT_STRAWBERRY, UnitEnum.GRAM, 600, 0.20);
	}

	public Map<IngredientEnum, Ingredient> getIngredientsInventory() {
		return ingredientsInventory;
	}

}
