package ec.com.drinks.services;

import ec.com.drinks.configuration.InventoryConfiguration;
import ec.com.drinks.enumeration.IngredientEnum;
import ec.com.drinks.model.Drink;
import ec.com.drinks.model.Ingredient;
import ec.com.drinks.util.ConstantUtil;

/**
 * @author jpatino
 * @version 1.0
 * 
 * Class that contains the necessary logic to manage the inventory.
 */
public class InventoryService extends InventoryConfiguration {

	public void printInventory() {
		System.out.println("****************************************************************");
		System.out.println("************** CURRENT INVENTORY *******************************");
		System.out.println("****************************************************************");
		System.out.println("");

		StringBuilder dataInventory = new StringBuilder();
		dataInventory.append(String.format(ConstantUtil.FORMAT_SPACE_COLUMNS, "INGREDIENT"))
				.append(String.format(ConstantUtil.FORMAT_SPACE_COLUMNS, "UNIT")).append(String.format(ConstantUtil.FORMAT_SPACE_COLUMNS, "QUANTITY"))
				.append(String.format(ConstantUtil.FORMAT_SPACE_COLUMNS, "PRICE BY UNIT")).append(ConstantUtil.NEW_LINE);

		dataInventory.append(String.format(ConstantUtil.FORMAT_SPACE_COLUMNS, "").replace(" ", "-"))
				.append(String.format(ConstantUtil.FORMAT_SPACE_COLUMNS, "").replace(" ", "-"))
				.append(String.format(ConstantUtil.FORMAT_SPACE_COLUMNS, "").replace(" ", "-"))
				.append(String.format(ConstantUtil.FORMAT_SPACE_COLUMNS, "").replace(" ", "-")).append(ConstantUtil.NEW_LINE);

		for (Ingredient ingredient : getIngredientsInventory().values()) {
			dataInventory.append(String.format(ConstantUtil.FORMAT_SPACE_COLUMNS, ingredient.getCode()))
					.append(String.format(ConstantUtil.FORMAT_SPACE_COLUMNS, ingredient.getUnit()))
					.append(String.format(ConstantUtil.FORMAT_SPACE_COLUMNS, ingredient.getQuantity()))
					.append(String.format(ConstantUtil.FORMAT_SPACE_COLUMNS, ingredient.getPriceByUnit())).append(ConstantUtil.NEW_LINE);
		}

		System.out.println(dataInventory.toString());
		System.out.println(ConstantUtil.RETURN_MESSAGE);
	}

	public void updateInventory(Drink drink) {
		updateIngredient(drink.getIce(), IngredientEnum.ICE);
		updateIngredient(drink.getSugar(), IngredientEnum.SUGAR);
		updateIngredient(drink.getCondensedMilk(), IngredientEnum.CONDENSED_MILK);

		switch (drink.getFruitType()) {
		case FRUIT_BANANA:
			updateIngredient(drink.getFruit(), IngredientEnum.FRUIT_BANANA);
			break;

		case FRUIT_MANGO:
			updateIngredient(drink.getFruit(), IngredientEnum.FRUIT_MANGO);
			break;

		case FRUIT_STRAWBERRY:
			updateIngredient(drink.getFruit(), IngredientEnum.FRUIT_STRAWBERRY);
			break;

		case FRUIT_BANANA_MANGO:
			updateIngredient(drink.getFruit(), IngredientEnum.FRUIT_BANANA);
			updateIngredient(drink.getFruitMixed(), IngredientEnum.FRUIT_MANGO);
			break;

		default:
			break;
		}
	}

	private void updateIngredient(int quantity, IngredientEnum typeIngredient) {
		Ingredient ingredient = getIngredientsInventory().get(typeIngredient);
		ingredient.setQuantity(ingredient.getQuantity() - quantity);
		getIngredientsInventory().put(typeIngredient, ingredient);
	}
	
	public boolean inStock(IngredientEnum typeIngredient, int sale) {
		Ingredient ingredient = getIngredientsInventory().get(typeIngredient);
		if(sale > ingredient.getQuantity()) {
			return false;
		}else {
			return true;
		}
	}

	public boolean inStockByFourDrink(IngredientEnum typeIngredient, int sale) {
		Ingredient ingredient = getIngredientsInventory().get(typeIngredient);
		if(ingredient.getQuantity() < (sale * 4)) {
			return false;
		}else {
			return true;
		}
	}

}
