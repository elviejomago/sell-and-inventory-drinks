package ec.com.drinks.services;

import java.util.ArrayList;
import java.util.List;

import ec.com.drinks.builder.BananaDrinkBuilder;
import ec.com.drinks.builder.BananaMangoDrinkBuilder;
import ec.com.drinks.builder.DrinkBuilder;
import ec.com.drinks.builder.MangoDrinkBuilder;
import ec.com.drinks.builder.StrawberryDrinkBuilder;
import ec.com.drinks.enumeration.IngredientEnum;
import ec.com.drinks.enumeration.SizeEnum;
import ec.com.drinks.exception.NoEnoughIngredientsException;
import ec.com.drinks.model.Drink;
import ec.com.drinks.util.ConstantUtil;

/**
 * @author jpatino
 * @version 1.0
 * 
 * Class that contains the necessary logic to manage sales.
 */
public class SellService {

	private DrinkBuilder drinkBuilder;
	private List<Drink> reportDailySales;

	public SellService() {
		this.reportDailySales = new ArrayList<Drink>();
	}

	public String sellDrink(SizeEnum size, IngredientEnum typeFruit, InventoryService inventoryService) throws NoEnoughIngredientsException {
		if(size == null) {
			return ConstantUtil.OPTION_NO_FOUND_MESSAGE;
		}
		
		switch (typeFruit) {
		case FRUIT_BANANA:
			drinkBuilder = new BananaDrinkBuilder();
			break;

		case FRUIT_MANGO:
			drinkBuilder = new MangoDrinkBuilder();
			break;

		case FRUIT_STRAWBERRY:
			drinkBuilder = new StrawberryDrinkBuilder();
			break;

		case FRUIT_BANANA_MANGO:
			drinkBuilder = new BananaMangoDrinkBuilder();
			break;

		default:
			break;
		}

		String message = "";
		try {
			message = buildDrink(size, inventoryService);
		} catch (NoEnoughIngredientsException e) {
			return e.getMessage();
		}

		inventoryService.updateInventory(drinkBuilder.getDrink());
		getReportDailySales().add(drinkBuilder.getDrink());

		return ConstantUtil.SUCCESS_SALE+message;
	}

	private String buildDrink(SizeEnum size, InventoryService inventoryService) throws NoEnoughIngredientsException {
		double price = 0.0;
		String message = "";
		drinkBuilder.newDrink(size);
		drinkBuilder.addBlendedFruit();
		message += validateInventoryFruit(inventoryService, drinkBuilder.getDrink().getFruitType());
		price += getDrinkPriceFruit(inventoryService);
		drinkBuilder.addCondencedMilk();
		message += validateInventory(inventoryService, IngredientEnum.CONDENSED_MILK, drinkBuilder.getDrink().getCondensedMilk());
		price += getDrinkPrice(inventoryService, IngredientEnum.CONDENSED_MILK, drinkBuilder.getDrink().getCondensedMilk());
		drinkBuilder.addIce();
		message += validateInventory(inventoryService, IngredientEnum.ICE, drinkBuilder.getDrink().getIce());
		price += getDrinkPrice(inventoryService, IngredientEnum.ICE, drinkBuilder.getDrink().getIce());
		drinkBuilder.addSugar();
		message += validateInventory(inventoryService, IngredientEnum.SUGAR, drinkBuilder.getDrink().getSugar());
		price += getDrinkPrice(inventoryService, IngredientEnum.SUGAR, drinkBuilder.getDrink().getSugar());

		drinkBuilder.getDrink().setPrice(price);
		
		return message;
	}

	private String validateInventoryFruit(InventoryService inventoryService, IngredientEnum typeIngredient) throws NoEnoughIngredientsException {
		String message = "";
		if(typeIngredient.equals(IngredientEnum.FRUIT_BANANA_MANGO)) {
			message += validateInventory(inventoryService, IngredientEnum.FRUIT_BANANA, drinkBuilder.getDrink().getFruit());
			message += validateInventory(inventoryService, IngredientEnum.FRUIT_MANGO, drinkBuilder.getDrink().getFruitMixed());
		}else {
			message += validateInventory(inventoryService, drinkBuilder.getDrink().getFruitType(), drinkBuilder.getDrink().getFruit());	
		}
		
		return message;
	}

	private String validateInventory(InventoryService inventoryService, IngredientEnum typeIngredient, int sale) throws NoEnoughIngredientsException {
		boolean inStock = inventoryService.inStock(typeIngredient, sale);
		if (!inStock) {
			throw new NoEnoughIngredientsException(ConstantUtil.NO_ENOUGH_INGREDIENTS_MESSAGE+" - "+typeIngredient);
		}

		boolean inStockFourDrinks = inventoryService.inStockByFourDrink(typeIngredient, sale);
		if (!inStockFourDrinks) {
			return ConstantUtil.NO_ENOUGH_INGREDIENTS_FOUR_DRINKS_MESSAGE.replace("{size}", drinkBuilder.getDrink().getSize().name())+" - "+typeIngredient;
		}
		
		return "";
	}

	private double getDrinkPrice(InventoryService inventoryService, IngredientEnum typeIngredient, int sale) {
		return inventoryService.getIngredientsInventory().get(typeIngredient).getPriceByUnit()*sale;
	}

	private double getDrinkPriceFruit(InventoryService inventoryService) {
		double price = 0.0;
		if(drinkBuilder.getDrink().getFruitType().equals(IngredientEnum.FRUIT_BANANA_MANGO)) {
			price = inventoryService.getIngredientsInventory().get(IngredientEnum.FRUIT_BANANA).getPriceByUnit()*drinkBuilder.getDrink().getFruit();
			price += inventoryService.getIngredientsInventory().get(IngredientEnum.FRUIT_MANGO).getPriceByUnit()*drinkBuilder.getDrink().getFruitMixed();
		}else {
			price = inventoryService.getIngredientsInventory().get(drinkBuilder.getDrink().getFruitType()).getPriceByUnit()*drinkBuilder.getDrink().getFruit();
		}
		
		return price;
	}

	public List<Drink> getReportDailySales() {
		return reportDailySales;
	}

	public void printReportDailySales() {
		System.out.println("****************************************************************");
		System.out.println("************** REPORT DAILY SALES ******************************");
		System.out.println("****************************************************************");
		System.out.println("");

		StringBuilder dataReport = new StringBuilder();
		dataReport.append(String.format(ConstantUtil.FORMAT_SPACE_COLUMNS, "FRUIT DRINK"))
				.append(String.format(ConstantUtil.FORMAT_SPACE_COLUMNS, "SIZE")).append(String.format(ConstantUtil.FORMAT_SPACE_COLUMNS, "PRICE"))
				.append(ConstantUtil.NEW_LINE);

		dataReport.append(String.format(ConstantUtil.FORMAT_SPACE_COLUMNS, "").replace(" ", "-"))
				.append(String.format(ConstantUtil.FORMAT_SPACE_COLUMNS, "").replace(" ", "-"))
				.append(String.format(ConstantUtil.FORMAT_SPACE_COLUMNS, "").replace(" ", "-")).append(ConstantUtil.NEW_LINE);

		for (Drink drink : getReportDailySales()) {
			dataReport.append(String.format(ConstantUtil.FORMAT_SPACE_COLUMNS, drink.getFruitType()))
					.append(String.format(ConstantUtil.FORMAT_SPACE_COLUMNS, drink.getSize()))
					.append(String.format(ConstantUtil.FORMAT_SPACE_COLUMNS, drink.getPrice())).append(ConstantUtil.NEW_LINE);
		}

		System.out.println(dataReport.toString());
		System.out.println(ConstantUtil.RETURN_MESSAGE);
	}

}
